# translation of kaccess.po to Latvian
# translation of kaccess.po to
# Copyright (C) 2007, 2008 Free Software Foundation, Inc.
#
#  Anita Reitere <nita@cc.lv>, 2007.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2007, 2008.
# Viesturs Zariņš <viesturs.zarins@mii.lu.lv>, 2009.
# Maris Nartiss <maris.kde@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kaccess\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-04 02:17+0000\n"
"PO-Revision-Date: 2019-12-07 14:00+0200\n"
"Last-Translator: Maris Nartiss <maris.kde@gmail.com>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.08.3\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andris Maziks, Anita Reitere, Viesturs Zariņš"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andris.m@delfi.lv, nita@cc.lv, viesturs.zarins@mii.lu.lv"

#: kaccess.cpp:65
msgid ""
"The Shift key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Shift taustiņš ir fiksēts un būs aktīvs katru reizi, kad piespiež kādu "
"taustiņu."

#: kaccess.cpp:66
msgid "The Shift key is now active."
msgstr "Shift taustiņš tagad ir aktīvs."

#: kaccess.cpp:67
msgid "The Shift key is now inactive."
msgstr "Shift taustiņš tagad ir neaktīvs."

#: kaccess.cpp:71
msgid ""
"The Control key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Control taustiņš ir fiksēts un būs aktīvs katru reizi, kad piespiež kādu "
"taustiņu."

#: kaccess.cpp:72
msgid "The Control key is now active."
msgstr "Control taustiņš tagad ir aktīvs."

#: kaccess.cpp:73
msgid "The Control key is now inactive."
msgstr "Control taustiņš tagad ir neaktīvs."

#: kaccess.cpp:77
msgid ""
"The Alt key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Alt taustiņš ir fiksēts un būs aktīvs katru reizi, kad piespiež kādu "
"taustiņu."

#: kaccess.cpp:78
msgid "The Alt key is now active."
msgstr "Alt taustiņš tagad ir aktīvs."

#: kaccess.cpp:79
msgid "The Alt key is now inactive."
msgstr "Alt taustiņš tagad ir neaktīvs."

#: kaccess.cpp:83
msgid ""
"The Win key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Win taustiņš ir fiksēts un būs aktīvs katru reizi, kad piespiež kādu "
"taustiņu."

#: kaccess.cpp:84
msgid "The Win key is now active."
msgstr "Win taustiņš tagad ir aktīvs."

#: kaccess.cpp:85
msgid "The Win key is now inactive."
msgstr "Win taustiņš tagad ir neaktīvs."

#: kaccess.cpp:89
msgid ""
"The Meta key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Meta taustiņš ir fiksēts un būs aktīvs katru reizi, kad piespiež kādu "
"taustiņu."

#: kaccess.cpp:90
msgid "The Meta key is now active."
msgstr "Meta taustiņš tagad ir aktīvs."

#: kaccess.cpp:91
msgid "The Meta key is now inactive."
msgstr "Meta taustiņš tagad ir neaktīvs."

#: kaccess.cpp:95
msgid ""
"The Super key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Super taustiņš ir fiksēts un būs aktīvs katru reizi, kad piespiež kādu "
"taustiņu."

#: kaccess.cpp:96
msgid "The Super key is now active."
msgstr "Super taustiņš tagad ir aktīvs."

#: kaccess.cpp:97
msgid "The Super key is now inactive."
msgstr "Super taustiņš tagad ir neaktīvs."

#: kaccess.cpp:101
msgid ""
"The Hyper key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Hyper taustiņš ir fiksēts un būs aktīvs katru reizi, kad piespiež kādu "
"taustiņu."

#: kaccess.cpp:102
msgid "The Hyper key is now active."
msgstr "Hyper taustiņš tagad ir aktīvs."

#: kaccess.cpp:103
msgid "The Hyper key is now inactive."
msgstr "Hyper taustiņš tagad ir neaktīvs."

#: kaccess.cpp:107
msgid ""
"The Alt Graph key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Alt Graph taustiņš ir fiksēts un būs aktīvs katru reizi, kad piespiež kādu "
"taustiņu."

#: kaccess.cpp:108
msgid "The Alt Graph key is now active."
msgstr "Alt Graph taustiņš tagad ir aktīvs."

#: kaccess.cpp:109
msgid "The Alt Graph key is now inactive."
msgstr "Alt Graph taustiņš tagad ir neaktīvs."

#: kaccess.cpp:110
msgid "The Num Lock key has been activated."
msgstr "Num Lock taustiņš ir aktivizēts."

#: kaccess.cpp:110
msgid "The Num Lock key is now inactive."
msgstr "Num Lock taustiņš tagad ir neaktīvs."

#: kaccess.cpp:111
msgid "The Caps Lock key has been activated."
msgstr "Caps Lock taustiņš ir aktivizēts."

#: kaccess.cpp:111
msgid "The Caps Lock key is now inactive."
msgstr "Caps Lock taustiņš tagad ir neaktīvs."

#: kaccess.cpp:112
msgid "The Scroll Lock key has been activated."
msgstr "Scroll Lock taustiņš ir aktivizēts."

#: kaccess.cpp:112
msgid "The Scroll Lock key is now inactive."
msgstr "Scroll Lock taustiņš tagad ir neaktīvs."

#: kaccess.cpp:329
#, kde-format
msgid "Toggle Screen Reader On and Off"
msgstr "Ieslēgt/izslēgt ekrāna lasītāju"

#: kaccess.cpp:331
#, kde-format
msgctxt "Name for kaccess shortcuts category"
msgid "Accessibility"
msgstr "Pieejamība"

#: kaccess.cpp:643
#, kde-format
msgid "AltGraph"
msgstr "AltGraph"

#: kaccess.cpp:645
#, kde-format
msgid "Hyper"
msgstr "Hiper"

#: kaccess.cpp:647
#, kde-format
msgid "Super"
msgstr "Super"

#: kaccess.cpp:649
#, kde-format
msgid "Meta"
msgstr "Meta"

#: kaccess.cpp:666
#, kde-format
msgid "Warning"
msgstr "Brīdinājums"

#: kaccess.cpp:694
#, kde-format
msgid "&When a gesture was used:"
msgstr "&Kad izmantoja žestu:"

#: kaccess.cpp:700
#, kde-format
msgid "Change Settings Without Asking"
msgstr "Mainīt iestatījumus nejautājot."

#: kaccess.cpp:701
#, kde-format
msgid "Show This Confirmation Dialog"
msgstr "Rādīt šo apstiprināšanas dialogu"

#: kaccess.cpp:702
#, kde-format
msgid "Deactivate All AccessX Features & Gestures"
msgstr "Atslēgt visas AccessX iespējas un žestus"

#: kaccess.cpp:745 kaccess.cpp:747
#, kde-format
msgid "Slow keys"
msgstr "Lēnie taustiņi"

#: kaccess.cpp:750 kaccess.cpp:752
#, kde-format
msgid "Bounce keys"
msgstr "Atlecošie taustiņi"

#: kaccess.cpp:755 kaccess.cpp:757
#, kde-format
msgid "Sticky keys"
msgstr "Lipīgie taustiņi"

#: kaccess.cpp:760 kaccess.cpp:762
#, kde-format
msgid "Mouse keys"
msgstr "Peles taustiņi"

#: kaccess.cpp:769
#, kde-format
msgid "Do you really want to deactivate \"%1\"?"
msgstr "Vai tiešām vēlaties atslēgt \"%1\"?"

#: kaccess.cpp:772
#, kde-format
msgid "Do you really want to deactivate \"%1\" and \"%2\"?"
msgstr "Vai tiešām vēlaties atslēgt \"%1\" un \"%2\"?"

#: kaccess.cpp:776
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\" and \"%3\"?"
msgstr "Vai tiešām vēlaties atslēgt \"%1\", \"%2\" un \"%3\"?"

#: kaccess.cpp:779
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "Vai tiešām vēlaties atslēgt \"%1\", \"%2\", \"%3\" un \"%4\"?"

#: kaccess.cpp:790
#, kde-format
msgid "Do you really want to activate \"%1\"?"
msgstr "Vai tiešām vēlaties aktivizēt \"%1\"?"

#: kaccess.cpp:793
#, kde-format
msgid "Do you really want to activate \"%1\" and to deactivate \"%2\"?"
msgstr "Vai tiešām vēlaties aktivizēt \"%1\" un atslēgt \"%2\"?"

#: kaccess.cpp:796
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\" and \"%3\"?"
msgstr "Vai tiešām vēlaties aktivizēt \"%1\" un atslēgt \"%2\"un \"%3\"?"

#: kaccess.cpp:802
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\", \"%3\" and "
"\"%4\"?"
msgstr ""
"Vai tiešām vēlaties aktivizēt \"%1\" un atslēgt \"%2\", \"%3\" un \"%4\"?"

#: kaccess.cpp:813
#, kde-format
msgid "Do you really want to activate \"%1\" and \"%2\"?"
msgstr "Vai tiešām vēlaties aktivizēt \"%1\" un \"%2\"?"

#: kaccess.cpp:816
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and \"%2\" and to deactivate \"%3\"?"
msgstr "Vai tiešām vēlaties aktivizēt \"%1\" un \"%2\" un atslēgt \"%3\"?"

#: kaccess.cpp:822
#, kde-format
msgid ""
"Do you really want to activate \"%1\", and \"%2\" and to deactivate \"%3\" "
"and \"%4\"?"
msgstr ""
"Vai tiešām vēlaties aktivizēt \"%1\" un \"%2\" un atslēgt \"%3\" un \"%4\"?"

#: kaccess.cpp:833
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\" and \"%3\"?"
msgstr "Vai tiešām vēlaties aktivizēt \"%1\", \"%2\" un \"%3\"?"

#: kaccess.cpp:836
#, kde-format
msgid ""
"Do you really want to activate \"%1\", \"%2\" and \"%3\" and to deactivate "
"\"%4\"?"
msgstr ""
"Vai tiešām vēlaties aktivizēt \"%1\", \"%2\" un \"%3\" un atslēgt \"%4\"?"

#: kaccess.cpp:845
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "Vai tiešām vēlaties aktivizēt \"%1\", \"%2\", \"%3\" un \"%4\"?"

#: kaccess.cpp:854
#, kde-format
msgid "An application has requested to change this setting."
msgstr "Kāda programma prasa mainīt šo iestatījumu."

#: kaccess.cpp:858
#, kde-format
msgid ""
"You held down the Shift key for 8 seconds or an application has requested to "
"change this setting."
msgstr ""
"Jūs turējāt Shift taustiņu nospiestu 8 sekundes, vai kāda programma prasa "
"mainīt šo iestatījumu."

#: kaccess.cpp:860
#, kde-format
msgid ""
"You pressed the Shift key 5 consecutive times or an application has "
"requested to change this setting."
msgstr ""
"Jūs nospiedāt Shift taustiņu 5 reizes pēc kārtas, vai kāda programma prasa "
"mainīt šo iestatījumu."

#: kaccess.cpp:864
#, kde-format
msgid "You pressed %1 or an application has requested to change this setting."
msgstr "Jūs nospiedāt %1, vai kāda programma prasa mainīt šo iestatījumu."

#: kaccess.cpp:869
#, kde-format
msgid ""
"An application has requested to change these settings, or you used a "
"combination of several keyboard gestures."
msgstr ""
"Kāda programma prasa mainīt šos iestatījumus, vai arī jūs izmantojāt vairāku "
"tastatūras žestu kombināciju."

#: kaccess.cpp:871
#, kde-format
msgid "An application has requested to change these settings."
msgstr "Kāda programma prasa mainīt šos iestatījumus."

#: kaccess.cpp:876
#, kde-format
msgid ""
"These AccessX settings are needed for some users with motion impairments and "
"can be configured in the KDE System Settings. You can also turn them on and "
"off with standardized keyboard gestures.\n"
"\n"
"If you do not need them, you can select \"Deactivate all AccessX features "
"and gestures\"."
msgstr ""
"Šie AccessX iestatījumi ir vajadzīgi dažiem lietotājiem ar kustību "
"traucējumiem, un tos var konfigurēt KDE sistēmas iestatījumos. Tos var arī "
"ieslēgt un izslēgt ar standarta tastatūras žestiem.\n"
"\n"
"Ja tie jums nav vajadzīgi, varat izvēlēties \"Atslēgt visas AccessX iespējas "
"un žestus\"."

#: kaccess.cpp:897
#, kde-format
msgid ""
"Slow keys has been enabled. From now on, you need to press each key for a "
"certain length of time before it gets accepted."
msgstr ""
"Lēnie taustiņi ir ieslēgti. Tagad katrs taustiņš jātur noteiktu laiku, pirms "
"to reģistrē."

#: kaccess.cpp:899
#, kde-format
msgid "Slow keys has been disabled."
msgstr "Lēnie taustiņi ir atslēgti."

#: kaccess.cpp:903
#, kde-format
msgid ""
"Bounce keys has been enabled. From now on, each key will be blocked for a "
"certain length of time after it was used."
msgstr ""
"Atlecošie taustiņi ir ieslēgti. Tagad katrs taustiņš tiks bloķēts uz "
"noteiktu laiku pēc lietošanas."

#: kaccess.cpp:905
#, kde-format
msgid "Bounce keys has been disabled."
msgstr "Atlecošie taustiņi ir atslēgti."

#: kaccess.cpp:909
#, kde-format
msgid ""
"Sticky keys has been enabled. From now on, modifier keys will stay latched "
"after you have released them."
msgstr ""
"Lipīgie taustiņi ir ieslēgti. Tagad modificējošie taustiņi paliks fiksēti "
"pēc to atlaišanas."

#: kaccess.cpp:911
#, kde-format
msgid "Sticky keys has been disabled."
msgstr "Lipīgie taustiņi ir atslēgti."

#: kaccess.cpp:915
#, kde-format
msgid ""
"Mouse keys has been enabled. From now on, you can use the number pad of your "
"keyboard in order to control the mouse."
msgstr ""
"Peles taustiņi ir ieslēgti. Tagad jūs varat izmantot savu cipartastatūru "
"peles vadīšanai."

#: kaccess.cpp:917
#, kde-format
msgid "Mouse keys has been disabled."
msgstr "Peles taustiņi ir atslēgti"

#: main.cpp:36
#, fuzzy, kde-format
#| msgctxt "Name for kaccess shortcuts category"
#| msgid "Accessibility"
msgid "Accessibility"
msgstr "Pieejamība"

#: main.cpp:36
#, kde-format
msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
msgstr "(c) 2000, Matthias Hoelzer-Kluepfel"

#: main.cpp:38
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matthias Hoelzer-Kluepfel"

#: main.cpp:38
#, kde-format
msgid "Author"
msgstr "Autors"

#~ msgid "KDE Accessibility Tool"
#~ msgstr "KDE pieejamības rīks"

#~ msgid "kaccess"
#~ msgstr "kaccess"

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "Sticky Keys"
#~ msgstr "Lipīgie taustiņi"

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "Use &sticky keys"
#~ msgstr "Lipīgie taustiņi"

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "&Lock sticky keys"
#~ msgstr "Lipīgie taustiņi"

#, fuzzy
#~| msgid "Slow keys"
#~ msgid "&Use slow keys"
#~ msgstr "Lēnie taustiņi"

#, fuzzy
#~| msgid "Bounce keys"
#~ msgid "Bounce Keys"
#~ msgstr "Atlecošie taustiņi"

#, fuzzy
#~| msgid "Bounce keys"
#~ msgid "Use bou&nce keys"
#~ msgstr "Atlecošie taustiņi"
