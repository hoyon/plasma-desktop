# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2015, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-22 02:07+0000\n"
"PO-Revision-Date: 2022-02-04 15:37+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.2\n"

#: contents/ui/ToolBoxContent.qml:267
#, kde-format
msgid "Choose Global Theme…"
msgstr "Alege tematica globală…"

#: contents/ui/ToolBoxContent.qml:274
#, kde-format
msgid "Configure Display Settings…"
msgstr "Configurare parametri ecran…"

#: contents/ui/ToolBoxContent.qml:295
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr ""

#: contents/ui/ToolBoxContent.qml:310
#, kde-format
msgid "Exit Edit Mode"
msgstr "Ieși din regim de modificare"

#~ msgid "Leave"
#~ msgstr "Pleacă"
