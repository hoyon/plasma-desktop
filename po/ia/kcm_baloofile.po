# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Giovanni Sora <g.sora@tioscali.it>, 2014.
# G.Sora <g.sora@tiscali.it>, 2014, 2016, 2017, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-04 02:17+0000\n"
"PO-Revision-Date: 2023-05-31 22:02+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.1\n"

#: ui/main.qml:50
#, kde-format
msgid "Pause Indexer"
msgstr "Pausa Indicisator"

#: ui/main.qml:50
#, kde-format
msgid "Resume Indexer"
msgstr "Recomencia indicisator"

#: ui/main.qml:78
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Isto dishabilitara cerca de file in menus de KRunner e lanceator,e removera "
"monstrator de metadatos extendite ex omne applicationes de KDE. "

#: ui/main.qml:87
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Tu vole deler le datos e indice slveguardate? %1 de spatio essera liberate, "
"ma si indicisar es habilitate depost, le integre indice deber esser recreate "
"ex nihil. Isto pote prender alcun tempore, dependente de quante files tu ha."

#: ui/main.qml:89
#, kde-format
msgid "Delete Index Data"
msgstr "Dele Datos de indice"

#: ui/main.qml:109
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr ""
"Le systema debe esser re-initiate ante que iste cambiamentos  potera haber "
"effecto."

#: ui/main.qml:113
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Re-Initia"

#: ui/main.qml:120
#, kde-format
msgid ""
"File Search helps you quickly locate your files. You can choose which "
"folders and what types of file data are indexed."
msgstr ""
"Cerca de File adjuta te localisar rapidemente tu files. Tu pote seliger qual "
"dossieres e que typos de datos de file es indicisate."

#: ui/main.qml:135
#, kde-format
msgctxt "@title:group"
msgid "File indexing:"
msgstr "Indicisation de file:"

#: ui/main.qml:136
#, kde-format
msgctxt "@action:check"
msgid "Enabled"
msgstr "Habilitate"

#: ui/main.qml:150
#, kde-format
msgctxt "@label indexing status"
msgid "Status:"
msgstr "Stato:"

#: ui/main.qml:154
#, kde-format
msgctxt "State and a percentage of progress"
msgid "%1, %2% complete"
msgstr "%1, %2% complete"

#: ui/main.qml:167
#, kde-format
msgctxt "@label file currently being indexed"
msgid "Currently indexing:"
msgstr "Indicisante currentemente :"

#: ui/main.qml:177
#, kde-kuit-format
msgctxt "@info Currently Indexing"
msgid "<filename>%1</filename>"
msgstr "<filename>%1</filename>"

#: ui/main.qml:193
#, kde-format
msgctxt "@title:group"
msgid "Data to index:"
msgstr "Datos a indice:"

#: ui/main.qml:195
#, kde-format
msgid "File names and contents"
msgstr "Nomines de files e contentos"

#: ui/main.qml:208
#, kde-format
msgid "File names only"
msgstr "Solmente nomine de files"

#: ui/main.qml:227
#, kde-format
msgid "Hidden files and folders"
msgstr "Files e dossieres celate"

#: ui/main.qml:283
#, kde-format
msgctxt "@title:table Locations to include or exclude from indexing"
msgid "Locations"
msgstr "Locationes"

#: ui/main.qml:288
#, kde-format
msgctxt "@action:button"
msgid "Start Indexing a Folder…"
msgstr "Initia indicisar un dossier..."

#: ui/main.qml:299
#, kde-format
msgctxt "@action:button"
msgid "Stop Indexing a Folder…"
msgstr "Stoppa indicisar un dossier..."

#: ui/main.qml:362
#, kde-format
msgid "Not indexed"
msgstr "Non indicisate"

#: ui/main.qml:363
#, kde-format
msgid "Indexed"
msgstr "Indicisate"

#: ui/main.qml:393
#, kde-format
msgid "Delete entry"
msgstr "Dele entrata"

#: ui/main.qml:408
#, kde-format
msgid "Select a folder to include"
msgstr "Selige un dossier de includer"

#: ui/main.qml:408
#, kde-format
msgid "Select a folder to exclude"
msgstr "Selige un dossier de excluder"

#~ msgid "Enable File Search"
#~ msgstr "Habilita Cerca de file"

#~ msgid "Also index file content"
#~ msgstr "Anque indicisa contento de file"

#~ msgid "Folder specific configuration:"
#~ msgstr "Configuration specific de dossier:"

#~ msgid ""
#~ "This module lets you configure the file indexer and search functionality."
#~ msgstr ""
#~ "Iste modulo te permitte configurr le functionalitate de indicisar e "
#~ "cercar."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Giovanni Sora"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "g.sora@tiscali.it"

#~ msgid "File Search"
#~ msgstr "Cerca de file"

#~ msgid "Copyright 2007-2010 Sebastian Trüg"
#~ msgstr "Copyright 2007-2010 Sebastian Trug"

#~ msgid "Sebastian Trüg"
#~ msgstr "Sebastian Trueg"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Tomaz Canabrava"
#~ msgstr "Tomaz Canabrava"

#~ msgid "Add folder configuration…"
#~ msgstr "Adde configuration de dossier..."

#, fuzzy
#~| msgid "Folder %1 is already excluded"
#~ msgid "%1 is excluded."
#~ msgstr "Dossier %1 ja es excludite"

#~ msgid "Do not search in these locations:"
#~ msgstr "Non cerca in iste locationes:"

#~ msgid "Select the folder which should be excluded"
#~ msgstr "Selige le dossier le qual on deberea excluder"

#~ msgid "Folder's parent %1 is already excluded"
#~ msgstr "Genitor %1 de dossier ja es excludite"

#~ msgid "Configure File Search"
#~ msgstr "Configura cerca de File"

#~ msgid "The root directory is always hidden"
#~ msgstr "Le directorio radice sempre es celate"
