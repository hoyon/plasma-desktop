# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2013.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-31 03:09+0000\n"
"PO-Revision-Date: 2017-05-16 06:57-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#: plasma-desktop-runner.cpp:24
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "desktop console"
msgstr "מסוף שולחן־עבודה"

#: plasma-desktop-runner.cpp:25
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "wm console"
msgstr "מסוף wm"

#: plasma-desktop-runner.cpp:28 plasma-desktop-runner.cpp:30
#, kde-format
msgid ""
"Opens the Plasma desktop interactive console with a file path to a script on "
"disk."
msgstr "פותח את המסוף האינטראקיבי עם תסריט שנקרא מתוך נתיב בדיסק."

#: plasma-desktop-runner.cpp:29
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "desktop console :q:"
msgstr "מסוף שולחן־עבודה :q:"

#: plasma-desktop-runner.cpp:31 plasma-desktop-runner.cpp:32
#, kde-format
msgid ""
"Opens the KWin interactive console with a file path to a script on disk."
msgstr "פותח את המסוף האינטראקטיבי של KWin עם תסריט שנקרא מתוך נתיב בדיסק."

#: plasma-desktop-runner.cpp:32
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "wm console :q:"
msgstr "מסוף wm‏ :q:"

#: plasma-desktop-runner.cpp:42
#, kde-format
msgid "Open Plasma desktop interactive console"
msgstr "פתח את המסוף האינטראקטיבי של Plasma Desktop"

#: plasma-desktop-runner.cpp:51
#, kde-format
msgid "Open KWin interactive console"
msgstr "פתח את המסוף האינטראקטיבי של KWin"
