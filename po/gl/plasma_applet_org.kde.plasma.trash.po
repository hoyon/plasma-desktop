# translation of plasma_applet_trash.po to galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# mvillarino <mvillarino@users.sourceforge.net>, 2008.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2012, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2016.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_trash\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-22 02:07+0000\n"
"PO-Revision-Date: 2023-06-18 13:00+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: contents/ui/main.qml:104
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "Abrir"

#: contents/ui/main.qml:105
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "Baleiro"

#: contents/ui/main.qml:111
#, kde-format
msgid "Trash Settings…"
msgstr "Configuración do lixo…"

#: contents/ui/main.qml:162
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"Lixo\n"
"Baleiro"

#: contents/ui/main.qml:162
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"Lixo\n"
"Un elemento"
msgstr[1] ""
"Lixo\n"
"%1 elementos"

#: contents/ui/main.qml:171
#, kde-format
msgid "Trash"
msgstr "Lixo"

#: contents/ui/main.qml:172
#, kde-format
msgid "Empty"
msgstr "Baleiro"

#: contents/ui/main.qml:172
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "Un elemento"
msgstr[1] "%1 elementos"

#~ msgid ""
#~ "Trash \n"
#~ " Empty"
#~ msgstr ""
#~ "Lixo \n"
#~ " Baleiro"

#~ msgid "Empty Trash"
#~ msgstr "Baleirar o lixo "

#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr "Desexa realmente baleirar o lixo? Hanse borrar todos os elementos."

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "&Empty Trashcan"
#~ msgstr "&Baleirar o lixo"

#~ msgid "&Menu"
#~ msgstr "&Menú"

#~ msgctxt "@title:window"
#~ msgid "Empty Trash"
#~ msgstr "Baleirar o lixo "

#~ msgid "Emptying Trashcan..."
#~ msgstr "Estáse a baleirar o lixo..."
